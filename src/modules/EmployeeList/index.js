import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, Button } from 'react-native';

import { ROUTES } from '../../data/config/constants';

class EmployeeList extends Component {
    render() {
        console.log('this.props', this.props);
        const { navigate } = this.props.navigation;
        return (
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <Text>EmployeeList</Text>
                <Button title='go to profile' onPress={() => navigate(ROUTES.PROFILE)} />
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        authDetails: state.authDetails
    };
}

export default connect(mapStateToProps)(EmployeeList);
