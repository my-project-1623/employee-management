import React, { Component } from 'react';
import { View, Text, Button } from 'react-native';

import { ROUTES } from '../../data/config/constants';

class Profile extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <Text>Profile</Text>
                <Button 
                    title='go to employees list' 
                    onPress={() => navigate(ROUTES.EMPLOYEE_LIST)} 
                />
            </View>
        );
    }
}

export default Profile;
