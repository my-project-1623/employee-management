import React, { Component } from 'react';
import { View, Text, AsyncStorage } from 'react-native';

import { ROUTES } from '../../data/config/constants';

class AuthLoading extends Component {
    componentWillMount() {
        const { navigate } = this.props.navigation;
        AsyncStorage.getItem('id_token').then((token) => {
            this.setState({ hasToken: token !== null });
            navigate(this.state.hasToken ? ROUTES.EMPLOYEE_LIST : ROUTES.PROFILE);
        });
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text>AuthLoading...</Text>
            </View>
        );
    }
}

export default AuthLoading;
