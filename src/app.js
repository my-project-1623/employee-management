import React, { Component } from 'react';
import { Provider } from 'react-redux';

import createReduxStore from './data/redux/store';
import AppContainer from './appContainer';

class App extends Component {
    constructor() {
        super();
        this.state = {
            hasToken: false
        };
    }
    
    render() {
        return (
            <Provider store={createReduxStore()}>
                <AppContainer />
            </Provider>
        );
    }
}

export default App;
