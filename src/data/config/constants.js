export const ROUTES = {
    PROFILE: 'PROFILE',
    EMPLOYEE_LIST: 'EMPLOYEE_LIST',
    AUTH_LOADING: 'AUTH_LOADING',
};
