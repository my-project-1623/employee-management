import * as actionTypes from './actionTypes';
import states from './states';

const authDetails = (state = states.authDetails, action) => {
    switch (action.type) {
        case actionTypes.UPDATE_AUTH_STATE:
            return {
                ...state,
                [action.payload.name]: action.payload.value,
            };

        default:
            return state;
    }
};

export default authDetails;

