import { createStore, applyMiddleware, compose } from 'redux';
import ReduxThunk from 'redux-thunk';
import { createReactNavigationReduxMiddleware } from 'react-navigation-redux-helpers';

import rootReducer from './rootReducer';

const navigationMiddleware = createReactNavigationReduxMiddleware(
    'root',
    state => state.nav,
);

const createReduxStore = () => {
    const middlewares = [
        // Add other middleware on this line...
        // thunk middleware can also accept an extra argument to be passed to each thunk action
        // https://github.com/gaearon/redux-thunk#injecting-a-custom-argument
        ReduxThunk,
        navigationMiddleware
    ];
    const store = createStore(
        rootReducer,
        {},
        compose(applyMiddleware(...middlewares))
    );
    return store;
};

export default createReduxStore;
