import { combineReducers } from 'redux';
import { createNavigationReducer } from 'react-navigation-redux-helpers';

import AppNavigator from '../../appNavigator';
import authDetails from './authDetails/reducers';

const navReducer = createNavigationReducer(AppNavigator);

const rootReducer = combineReducers({
    authDetails,
    nav: navReducer
});

export default rootReducer;
