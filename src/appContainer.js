import { reduxifyNavigator } from 'react-navigation-redux-helpers';
import { connect } from 'react-redux';
import AppNavigator from './appNavigator';

const App = reduxifyNavigator(AppNavigator, 'root');
const mapStateToProps = (state) => ({
  state: state.nav,
});

const AppContainer = connect(mapStateToProps)(App);
export default AppContainer;
