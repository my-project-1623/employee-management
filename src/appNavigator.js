import { createStackNavigator } from 'react-navigation';

import { ROUTES } from './data/config/constants';

import AuthLoading from './components/AuthLoading';
import EmployeeList from './modules/EmployeeList';
import Profile from './modules/Profile';

const AppNavigator = createStackNavigator(
    {
        [ROUTES.EMPLOYEE_LIST]: {
            screen: EmployeeList
        },
        [ROUTES.PROFILE]: {
            screen: Profile
        },
        [ROUTES.AUTH_LOADING]: {
            screen: AuthLoading
        },
    }
);

export default AppNavigator;
